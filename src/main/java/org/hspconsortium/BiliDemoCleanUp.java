package org.hspconsortium;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.model.api.IResource;
import ca.uhn.fhir.rest.client.IGenericClient;
import org.hl7.fhir.dstu3.model.Observation;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Subscription;

public class BiliDemoCleanUp {
    //private static String FHIR_SERVICE_ENDPOINT = "https://sandbox.hspconsortium.org/dstu2/open-hspc-reference-api/data";
    private static String FHIR_SERVICE_ENDPOINT = "http://localhost:8071/data";
    private static final IGenericClient hapiFhirClient = FhirContext.forDstu3().newRestfulGenericClient(FHIR_SERVICE_ENDPOINT);


    public static void main(String[] args) throws Exception{

        for (int i = 0; i< args.length; i++) {

            switch (args[i]){
                case "-h" :
                    System.out.println("java -jar hspc-tools.jar org.hspconsortium.DataLoad [options]");
                    System.out.println("   Options:");
                    System.out.println("   -h       print this message");
                    System.out.println("   -url     the url for the hspc api ex: -url http://localhost:8071/data");
                    return;
                case "-url" :
                    FHIR_SERVICE_ENDPOINT = args[++i];
                    break;
            }
        }
        deletePatients();
        deleteSubscriptions();
    }

    private static void deletePatients() {
        ca.uhn.fhir.model.api.Bundle results = hapiFhirClient
                .search()
                .forResource(Patient.class)
                .where(Patient.FAMILY.matches().value("LastName"))
                .execute();

        System.out.println("Patients " + results.size());
        for (IResource resource : results.toListOfResources()) {
            if (resource instanceof Patient) {
                deleteObservations((Patient)resource);
                hapiFhirClient.delete().resourceById(resource.getId()).execute();
            }
        }
    }

    private static void deleteObservations(Patient patient) {
        ca.uhn.fhir.model.api.Bundle results = hapiFhirClient
                .search()
                .forResource(Observation.class)
                .where(Observation.SUBJECT.hasId(patient.getId()))
                .execute();

        System.out.println("Observations " + results.size());
        for (IResource resource : results.toListOfResources()) {
            hapiFhirClient.delete().resourceById(resource.getId()).execute();
        }
    }

    private static void deleteSubscriptions() {
        ca.uhn.fhir.model.api.Bundle results = hapiFhirClient
                .search()
                .forResource(Subscription.class)
                .execute();

        System.out.println("Subscriptions " + results.size());
        for (IResource resource : results.toListOfResources()) {
            hapiFhirClient.delete().resourceById(resource.getId()).execute();
        }
    }
}
