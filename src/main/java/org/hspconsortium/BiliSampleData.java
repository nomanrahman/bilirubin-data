package org.hspconsortium;

import org.apache.commons.io.IOUtils;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class BiliSampleData {
    private static final String STATIC_DATA_PATH = "bilirubin";
    private static String OUTPUT_DIR = "output";
    private static final String DATE_TIME_START = "START-";
    private static final String DATE_TIME_END = "-END";
    private static final String PATIENTID = "-PATIENTID-";
    private static final String DATE = "-DATE-";

    private static String patientID;


    public static void main(String[] args) throws Exception{

        boolean userProvidedDir = false;
        boolean writeOutput = true;
        boolean canWrite = false;

        for (int i = 0; i< args.length; i++) {

            switch (args[i]){
                case "-h" :
                    System.out.println("java -jar hspc-tools.jar org.hspconsortium.DataLoad [options]");
                    System.out.println("   Options:");
                    System.out.println("   -h       print this message");
                    System.out.println("   -out     the output directory for results; default '<current dir>/output'");
                    System.out.println("            unless the current directory is 'target', the output directory will");
                    System.out.println("            be created one directory up from 'target'");
                    System.out.println("            NOTE: the import will fail if results can't be written out");
                    System.out.println("   -no-out  no output for results");
                    return;
                case "-out" :
                    userProvidedDir = true;
                    OUTPUT_DIR = args[++i];
                    break;
                case "-no-out" :
                    writeOutput = false;
                    break;
            }
        }

        //Test writing output
        if (userProvidedDir || writeOutput) {
            canWrite = checkOutput(OUTPUT_DIR, userProvidedDir);
            System.out.println("can write: " + canWrite);
            if (!canWrite ) {
                return; //Exit if results can't be written out
            }
        }

        PathMatchingResourcePatternResolver resourceResolver = new PathMatchingResourcePatternResolver();
        org.springframework.core.io.Resource[] patientFileResources =
                resourceResolver.getResources(String.format("classpath:%s/*", STATIC_DATA_PATH));

        patientID = getUinqueID();
        for(org.springframework.core.io.Resource templateFile : patientFileResources){
            if (!templateFile.isReadable()) {
                continue;
            }
            byte[] fileContents = IOUtils.toByteArray(templateFile.getInputStream());

            System.out.println("File: " + templateFile.toString());

            if (canWrite) {
                writeOutput(templateFile.getFilename(), replaceDates(fileContents));
            }
        }
    }

    private static byte[] replaceDates(byte[] fileInput) {

        Date date = new Date();
        date = decrementDate(date, 715);
        //String dateString = new SimpleDateFormat("yyyy-MM-dd").format(date);
        String dateString = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(date);

        String inputString = new String(fileInput);
        String finishedString = "";

        inputString = inputString.replace(PATIENTID, patientID);
        inputString = inputString.replace(DATE, dateString);

        while (inputString.indexOf(DATE_TIME_START) > 0) {
            int index = inputString.indexOf(DATE_TIME_START);
            finishedString = finishedString + inputString.substring(0,index);
            inputString = inputString.substring(index+6);

            index = inputString.indexOf(DATE_TIME_END);
            String intString = inputString.substring(0,index);

            finishedString = finishedString + incrementDateString(date, Integer.parseInt(intString));

            inputString = inputString.substring(index+4);
        }

        finishedString = finishedString + inputString;

        return finishedString.getBytes();
    }

    private static String incrementDateString(Date date, Integer minutes) {
        long ONE_MINUTE_IN_MILLIS=60000;//millisecs

        long t=date.getTime();
        Date newDate = new Date(t + (minutes * ONE_MINUTE_IN_MILLIS));
        return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(newDate);
    }

    private static String decrementDateString(Date date, Integer minutes) {
        long ONE_MINUTE_IN_MILLIS=60000;//millisecs

        long t=date.getTime();
        Date newDate = new Date(t - (minutes * ONE_MINUTE_IN_MILLIS));
        return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(newDate);
    }

    private static Date decrementDate(Date date, Integer minutes) {
        long ONE_MINUTE_IN_MILLIS=60000;//millisecs

        long t=date.getTime();
        return new Date(t - (minutes * ONE_MINUTE_IN_MILLIS));
    }

    private static void writeOutput(String fileName, byte[] fileOutput) {
        OutputStream outputStream = null;
        InputStream inputStream = new ByteArrayInputStream(fileOutput);

        try {
            String outputDir = OUTPUT_DIR + "/" + fileName;
            System.out.println("Output: " + outputDir);
            outputStream = new FileOutputStream(outputDir);
            IOUtils.copy(inputStream, outputStream);
        } catch (IOException ex) {
            // report
        } finally {
            try {outputStream.close(); inputStream.close();} catch (Exception ex) {}
        }
    }

    private static boolean checkOutput(String directoryName, boolean userProvidedDir) {
        boolean canWriteFiles = false;
        String workingDir = directoryName;
        if (!userProvidedDir) {
            workingDir = System.getProperty("user.dir");
            if (workingDir.endsWith("/target")) {
                workingDir = workingDir.substring(0, workingDir.lastIndexOf("/target")+1);
            }

            if (directoryName.equalsIgnoreCase("output")) {
                workingDir = workingDir + "/" + directoryName;
            }
        }
        File theDir = new File(workingDir);
        System.out.println("Output Directory: " + workingDir);


        // if the directory does not exist, create it
        if (!theDir.exists()) {
            System.out.println("creating directory: " + workingDir);

            try{
                theDir.mkdir();
                if (theDir.exists()) {
                    canWriteFiles = true;
                }
            } catch(SecurityException se){
                return false;
            }
            if(canWriteFiles) {
                System.out.println("DIR created");
            }
        } else {
            File testFile  = new File(workingDir + "/testfile");
            try {
                testFile.createNewFile();
                if (testFile.exists()) {
                    canWriteFiles = true;
                }
                testFile.deleteOnExit();
            } catch (IOException e) {
                return false;
            }
        }
        OUTPUT_DIR = workingDir;
        return canWriteFiles;
    }

    private static String getUinqueID(){
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

}
